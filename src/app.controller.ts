import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('test/:cmd')
  getHello(@Param('cmd') cmd): string {
    this.appService.getHello('ls');
    return cmd;
  }

  @Get('password/:user')
  getPassword(@Param('user') user) {
    this.appService.doSomeQuery(user, user);
  }
}
