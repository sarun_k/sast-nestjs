import { Injectable } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { execSync } from 'child_process';

@Injectable()
export class AppService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  async doSomeQuery(user: string, pass: string) {
    return this.dataSource.query(
      `SELECT * FROM users WHERE user = '${user}' AND pass = '${pass}'`,
    );
  }

  getHello(cmd: string): string {
    console.log(execSync(cmd).toString());
    return 'Hello World!';
  }
}
